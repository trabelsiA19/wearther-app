import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WeatherstackAppComponent } from './weatherstack-app.component';

describe('WeatherstackAppComponent', () => {
  let component: WeatherstackAppComponent;
  let fixture: ComponentFixture<WeatherstackAppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WeatherstackAppComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WeatherstackAppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
