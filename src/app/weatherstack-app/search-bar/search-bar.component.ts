import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { WeatherServiceService } from 'src/app/weatherService/weather-service.service';


@Component({
  selector: 'app-search-bar',
  templateUrl: './search-bar.component.html',
  styleUrls: ['./search-bar.component.css']
})
export class SearchBarComponent implements OnInit {
  cityName:string="Tunis"

  constructor(private apiWeatherServiceService:WeatherServiceService,) { }

  ngOnInit(): void {
  }

  getW(){


    this.apiWeatherServiceService.pushSearch(this.cityName);
  }

}
