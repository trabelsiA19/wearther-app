import { DatePipe, formatDate } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { WeatherServiceService } from '../weatherService/weather-service.service';
import { SearchBarComponent } from './search-bar/search-bar.component';

@Component({
  selector: 'app-weatherstack-app',
  templateUrl: './weatherstack-app.component.html',
  styleUrls: ['./weatherstack-app.component.css']
})
export class WeatherstackAppComponent implements OnInit {
  maDate = new Date();
  maDateTwo=new Date(); 
  maDatethree=new Date(); 

  constructor(   private datePipe: DatePipe,private apiWeatherServiceService:WeatherServiceService) { 
  this.apiWeatherServiceService.listenSearch().subscribe((m: any) => {
    this.getWearth(m);
  })

  }



  
  preview:any ; 
  ngOnInit(): void {
    this.getWearth("Tunis")
  }

  getWearth(city:any){
    this.preview=null
    let date1= this.datePipe.transform(this.maDate.setDate(this.maDate.getDate()-1), 'yyyy-MM-dd') ; 
    let date2= this.datePipe.transform(this.maDateTwo.setDate(this.maDateTwo.getDate()-2), 'yyyy-MM-dd')
    let date3= this.datePipe.transform(this.maDatethree.setDate(this.maDatethree.getDate()-3), 'yyyy-MM-dd') 
    this.apiWeatherServiceService.getWeatherstack(city,date1+";"+date2+";"+date3).subscribe(
      data=>{
     
         this.preview=data;


      }, 
      err=>{
   
      }
  
    ) 
  }
  
}
