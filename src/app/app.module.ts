// import * as $ from 'jquery';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  CommonModule,
  LocationStrategy,
  PathLocationStrategy
} from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { Routes, RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Approutes } from './app-routing.module';
import { AppComponent } from './app.component';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { PERFECT_SCROLLBAR_CONFIG } from 'ngx-perfect-scrollbar';
import { PerfectScrollbarConfigInterface } from 'ngx-perfect-scrollbar';
import { JwtModule, JwtModuleOptions } from '@auth0/angular-jwt';
import { WeatherstackAppComponent } from './weatherstack-app/weatherstack-app.component';
import { BookmarkedLocationsComponent } from './weatherstack-app/bookmarked-locations/bookmarked-locations.component';
import { SearchBarComponent } from './weatherstack-app/search-bar/search-bar.component';
import { WeatherDataPreviewComponent } from './weatherstack-app/weather-data-preview/weather-data-preview.component';
import { WeatherLocationComponent } from './weatherstack-app/weather-data-preview/weather-location/weather-location.component';
import { CurrentWeatherDataComponent } from './weatherstack-app/weather-data-preview/current-weather-data/current-weather-data.component';
import { HistoricalWeatherDataComponent } from './weatherstack-app/weather-data-preview/historical-weather-data/historical-weather-data.component';
import { FeatherModule } from 'angular-feather';
import {  Sun, Cloud, CloudRain, CloudSnow, Wind , Thermometer, Droplet, Slack , Heart, X} from 'angular-feather/icons';
import { HttpClientModule } from '@angular/common/http'; 
import {DatePipe} from '@angular/common';

const JWT_Module_Options: JwtModuleOptions = {
  config: {
      
  }
};


const icons = {
  Sun, Cloud, CloudRain, CloudSnow, Wind , Thermometer, Droplet , Slack ,Heart, X
};


const DEFAULT_PERFECT_SCROLLBAR_CONFIG: PerfectScrollbarConfigInterface = {
  suppressScrollX: true,
  wheelSpeed: 1,
  wheelPropagation: true,
  minScrollbarLength: 20
};   

@NgModule({
  declarations: [
    AppComponent,
    WeatherstackAppComponent,
    BookmarkedLocationsComponent,
    SearchBarComponent,
    WeatherDataPreviewComponent,
    WeatherLocationComponent,
    CurrentWeatherDataComponent,
    HistoricalWeatherDataComponent

  
  ],
  imports: [
    CommonModule,
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,
	PerfectScrollbarModule,
  FeatherModule.pick(icons),

  JwtModule.forRoot(JWT_Module_Options),

    NgbModule,
    RouterModule.forRoot(Approutes, { useHash: false, relativeLinkResolution: 'legacy' })
  ],
  providers: [
    {
  
      provide: LocationStrategy,
      useClass: PathLocationStrategy
    },
    DatePipe,
 
	{
      provide: PERFECT_SCROLLBAR_CONFIG,
      useValue: DEFAULT_PERFECT_SCROLLBAR_CONFIG
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
