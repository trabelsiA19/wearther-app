import { Component, OnInit } from '@angular/core';
import { WeatherServiceService } from 'src/app/weatherService/weather-service.service';

@Component({
  selector: 'app-bookmarked-locations',
  templateUrl: './bookmarked-locations.component.html',
  styleUrls: ['./bookmarked-locations.component.css']
})
export class BookmarkedLocationsComponent implements OnInit {

   listLocation:any=[]
  constructor( private apiWeatherServiceService:WeatherServiceService) {

    this.apiWeatherServiceService.listenLocation().subscribe((m: any) => {
        this.listLocation.push(m)
    })

   }

  ngOnInit(): void {
  }


  delete(index:any){
    if (index !== -1) {
        this.listLocation.splice(index, 1);
    } 
  }

  search(location:any){

    this.apiWeatherServiceService.pushSearch(location);
  }

}
