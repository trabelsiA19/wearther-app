
 
 
 
 
 
 
 
 
 
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Inject, Optional } from '@angular/core';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';



@Injectable({
  providedIn: 'root'
})
export class WeatherServiceService {
  protected basePath = environment.baseUrl;
  protected access_key=environment.access_key; 
  private _listnerSearch = new Subject<any>();
  private _listnerLocation = new Subject<any>();
  protected PathAdresses = environment.PathAdresses;
  public defaultHeaders = new HttpHeaders();


  constructor(
    protected httpClient: HttpClient,

  ) {
 
  }




   /**
    * Avoir l'historique 
    *
    * @param location location 
    * @param historical_dates dtate 
    * @param observe set whether or not to return the data Observable as the body, response or events. defaults to returning the body.
    * @param reportProgress flag to report request and response progress.
    */
    public getWeatherstack(location: string, historical_dates: string,  observe: any = 'body', reportProgress: boolean = false ): Observable<any> {
 
      if (location === null || location === undefined) {
        throw new Error('Required parameter location was null or undefined when calling getWeatherstack.');
      }
  
      if (historical_dates === null || historical_dates === undefined) {
        throw new Error('Required parameter historical dates  was null or undefined when calling getWeatherstack.');
      }
  
  
      return this.httpClient.request<any>('get', `${this.basePath}/historical?access_key=${this.access_key}&query=${location}&historical_date=${historical_dates}`,
        {
          observe,
          reportProgress
        }
      );
    }







  listenSearch(): Observable<any> {
    return this._listnerSearch.asObservable();
  }

  pushSearch(filterBy: any) {
    this._listnerSearch.next(filterBy);
  }


  listenLocation(): Observable<any> {
    return this._listnerLocation.asObservable();
  }

  pushLocation(filterBy: any) {
    this._listnerLocation.next(filterBy);
  }


  
  ngOnDestroy() {
    }

}
 
 
 
 
 
 
 