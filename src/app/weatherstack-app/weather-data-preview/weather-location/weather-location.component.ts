import { Component, Input, OnInit } from '@angular/core';
import { WeatherServiceService } from 'src/app/weatherService/weather-service.service';

@Component({
  selector: 'app-weather-location',
  templateUrl: './weather-location.component.html',
  styleUrls: ['./weather-location.component.css']
})
export class WeatherLocationComponent implements OnInit {

  constructor(private apiWeatherServiceService: WeatherServiceService) { }
  @Input() location:any ;  
  ngOnInit(): void {
  }


  Add(){
    this.apiWeatherServiceService.pushLocation(this.location);
  }
}
