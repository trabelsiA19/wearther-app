import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-historical-weather-data',
  templateUrl: './historical-weather-data.component.html',
  styleUrls: ['./historical-weather-data.component.css']
})
export class HistoricalWeatherDataComponent implements OnInit {
  constructor() { }
  @Input() historical:any ;  
  lastOneDay:any
  lastTwoDay:any
  lastThree:any
  ngOnInit(): void {


    

this.lastOneDay= this.historical[Object.keys(this.historical)[0]];
this.lastTwoDay= this.historical[Object.keys(this.historical)[1]];
this.lastThree= this.historical[Object.keys(this.historical)[2]];

  }

}
