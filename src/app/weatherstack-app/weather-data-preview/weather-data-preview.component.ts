import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-weather-data-preview',
  templateUrl: './weather-data-preview.component.html',
  styleUrls: ['./weather-data-preview.component.css']
})
export class WeatherDataPreviewComponent implements OnInit {


  @Input() preview:any  ; 

  current:any 
  location:any
  historical:any ; 
  constructor( ) { 

  }

  ngOnInit(): void {

  
 this.current=this.preview.current
 this.location=this.preview.location
 this.historical=this.preview.historical

    }

}
